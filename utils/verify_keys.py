import time 
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import serialization
from cryptography.exceptions import InvalidSignature


with open('keys\private.pem', 'r') as content_file:
    private_key = content_file.read()

with open('keys\public.pem', 'r') as content_file:
    public_key = content_file.read()

JWT_PRIVATE_KEY = private_key
JWT_PUBLIC_KEY = public_key

def verify_message(message: bytes, signature: bytes, public_key: bytes):
    public_key = serialization.load_pem_public_key(public_key)
    #serialization.lo
    try:
        public_key.verify(
            signature,
            message,
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            hashes.SHA256()
        )
        return True
    except InvalidSignature:
        return False

def sign_message(message: bytes, private_key: bytes):
    private_key = serialization.load_pem_private_key(
        private_key,
        password=None
    )
    signature = private_key.sign(
        message,
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        hashes.SHA256()
    )
    return signature

message = str(int(time.time())).encode()
signature = sign_message(message, JWT_PRIVATE_KEY.encode())
#print(signature)

# Verify it
is_verified = verify_message(message=message, signature=signature, public_key=JWT_PUBLIC_KEY.encode())
print("Does it match? (Debe ser True): ", is_verified)