# import json
# from datetime import datetime

# import pandas as pd

# lst_of_objects = []
# i=0 

# #Must exist a .xlsx file in same dir that this file
# df_depart = pd.read_csv('Ubigeo.xlsx - depart.csv', 
#     dtype={'codigo': str, 'depart': str})
# df_prov = pd.read_csv('Ubigeo.xlsx - prov.csv', dtype={'codigo': str, 'nombre': str})
# df_dist = pd.read_csv('Ubigeo.xlsx - dist.csv', dtype={'codigo': str, 'nombre': str})

# #For department fields
# while i<= len(df_depart)-1:
#     i +=1
#     ubigeo = {
#         "model": "registro.ubigeo",
#         "pk": df_depart['codigo'][i-1]+"0000",
#         "fields": {
#             "codigo_depart":df_depart['codigo'][i-1],
#             "codigo_prov": "00",
#             "codigo_dist": "00",
#             "denominacion": df_depart['depart'][i-1],
#             "es_vigente": True,
#             "usuario_registra": 1,
#             "fec_reg": datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
#             "es_eliminado": False,
#         }
#     }
    
#     lst_of_objects.append(ubigeo)

# #For provincia fields
# j=0
# while j<= len(df_prov['codigo'])-1:
#     j +=1
#     i +=1
#     ubigeo = {
#         "model": "registro.ubigeo",
#         "pk": df_prov['codigo'][j-1]+"00",
#         "fields": {
#             #"codigo_ubigeo": df_prov['codigo'][j-1]+"00",
#             "codigo_depart":df_prov['codigo'][j-1][0:2],
#             "codigo_prov": df_prov['codigo'][j-1][2:],
#             "codigo_dist": "00",
#             "denominacion": df_prov['nombre'][j-1],
#             "es_vigente": True,
#             "usuario_registra": 1,
#             "fec_reg": datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
#             "es_eliminado": False,
#         }
#     }
    
#     lst_of_objects.append(ubigeo)

# #For distrit fields
# k=0
# while k<= len(df_dist['codigo'])-1:
#     k +=1
#     i +=1
#     ubigeo = {
#         "model": "registro.ubigeo",
#         "pk": df_dist['codigo'][k-1],
#         "fields": {
#             #"codigo_ubigeo": df_dist['codigo'][k-1],
#             "codigo_depart":df_dist['codigo'][k-1][0:2],
#             "codigo_prov": df_dist['codigo'][k-1][2:4],
#             "codigo_dist": df_dist['codigo'][k-1][4:],
#             "denominacion": df_dist['nombre'][k-1],
#             "es_vigente": True,
#             "usuario_registra": 1,
#             "fec_reg": datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
#             "es_eliminado": False,
#         }
#     }
    
#     lst_of_objects.append(ubigeo)

# #name of file out
# with open('data-ubigeos.json', 'w', encoding='utf-8') as f:
#   json.dump(lst_of_objects, f, ensure_ascii=False, indent=4)