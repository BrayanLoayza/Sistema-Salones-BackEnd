import typing

from django.http import HttpRequest, Http404
from django_filters.rest_framework.filterset import FilterSet
from rest_framework.exceptions import ValidationError


def valid_params(
    request: HttpRequest,
    params: typing.List[str]=None,
    filterset: FilterSet=None,
    raise_exception=True
) -> bool:
    """
    Validate that the `request` contains params in `params` array and
    fields in `filterset` (using get_fields method).

    :param `request`: HttpRequest object
    :param `params` (optional): string array of custom params
    :param `filterset` (optional): FilterSet object
    :param `raise_exception` (optional): boolean that indicates must be
        raise a `ValidationError`. Default True.

    You at least must be send `params` or `filterset` attr else it raise
    a `AssertionException`. If `raise_exception` is False, the function
    returns a boolean.
    """
    # TODO: validate param types
    assert not (params is None and filterset is None), \
        "You must be pass a 'params' or 'filterset' attr."
    _params = []
    if params is not None:
        _params = params

    if filterset is not None:
        _params += [field for field, _ in filterset.get_fields().items()]

    for param in _params:
        if request.GET.get(param, None) is None:
            if raise_exception:
                raise ValidationError('Bad param filters.')
            return False
    return True
