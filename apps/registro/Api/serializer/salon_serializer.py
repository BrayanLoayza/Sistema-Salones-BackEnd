from pyexpat import model
import black
from rest_framework import serializers
from ...models import Salon, Fotos_salon
from rest_framework.exceptions import ValidationError


class Salon_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Salon
        fields = '__all__'

class Fotos_Serializer(serializers.ModelSerializer):
    class Meta:
        model = Fotos_salon
        fields = '__all__'


class RegistrarSalon(serializers.Serializer):
    nombre_salon = serializers.CharField(max_length=100)
    descripcion = serializers.CharField(max_length=300)
    aforo_maximo = serializers.IntegerField(max_digits=5)
    precio_por_dia = serializers.DecimalField(max_digits=14, decimal_places=4)
    region = serializers.CharField(max_length=8)
    provincia = serializers.CharField(max_length=8)
    distrito = serializers.CharField(max_length=8)
    direccion = serializers.CharField(max_length=100)
    telefono = serializers.CharField(max_length=12,blank=True)
    telefono_2 = serializers.CharField(max_length=12,blank=True)
    email_contacto = serializers.EmailField()
    otra_forma_de_contacto =  serializers.CharField(max_length=200)
    doc_identidad = erializers.CharField(max_length=11)
    
    #foto = serializers.ImageField(upload_to=get_salon_image, default='user/default.png')
    