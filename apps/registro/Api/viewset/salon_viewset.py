from rest_framework import viewsets
from ...models import Salon
from ...models import Fotos_salon
from ..serializer.salon_serializer import  Salon_Serializer, Fotos_Serializer
from utils import permissions as custom_permission
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

class SalonViewSet(viewsets.ModelViewSet):
    queryset = Salon.objects.all()
    serializer_class = Salon_Serializer
    permission_classes = [custom_permission.GroupPermission]
    permission_groups = {
        'list': [custom_permission.ALLOW_ANY],
        'retrieve': [custom_permission.ALLOW_ANY],
        'partial_update': [custom_permission.ALLOW_ANY],
        'destroy': [custom_permission.ALLOW_ANY],
        'create': [custom_permission.ALLOW_ANY],
    }

class FotosSalonViewSet(viewsets.ModelViewSet):
    queryset = Salon.objects.all()
    serializer_class = Salon_Serializer
    permission_classes = [custom_permission.GroupPermission]
    permission_groups = {
        'list': [custom_permission.ALLOW_ANY],
        'retrieve': [custom_permission.ALLOW_ANY],
        'partial_update': [custom_permission.ALLOW_ANY],
        'destroy': [custom_permission.ALLOW_ANY],
        'create': [custom_permission.ALLOW_ANY],
    }
    