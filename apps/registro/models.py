from django.db import models
import uuid
from django.utils import timezone
from unicodedata import decimal
from django.conf import settings
from utils.text import get_random_name

# Create your models here.


class Salon(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user",null=False)
    nombre_salon = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300)
    aforo_maximo = models.IntegerField(max_digits=5)
    precio_por_dia = models.DecimalField(max_digits=14, decimal_places=4)
    region = models.CharField(max_length=8)
    provincia = models.CharField(max_length=8)
    distrito = models.CharField(max_length=8)
    direccion = models.CharField(max_length=100)
    telefono = models.CharField(max_length=12, null=True, blank=True)
    telefono_2 = models.CharField(max_length=12, null=True, blank=True)
    email_contacto = models.EmailField()
    otra_forma_de_contacto =  models.CharField(max_length=200)
    fec_reg = models.DateTimeField(auto_now_add=True)
    usuario_mod = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_mod")
    fec_mod = models.DateTimeField(auto_now=True)
    es_eliminado = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'registro\".\"salon'
        ordering = ['-fec_reg']
        get_latest_by = '-fec_reg'


def get_salon_image(instance, filename):
        return 'salon_image/{0}'.format(get_random_name(filename))

class Fotos_salon(models.Model):

    salon = models.ForeignKey(Salon, on_delete=models.CASCADE)
    foto = models.ImageField(upload_to=get_salon_image)
    fec_reg = models.DateTimeField(auto_now_add=True)
    usuario_mod = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_mod")
    fec_mod = models.DateTimeField(auto_now=True)
    es_eliminado = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)


