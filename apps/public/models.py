from unittest.util import _MAX_LENGTH
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser, Group, BaseUserManager
from django.utils.translation import gettext_lazy as _
from django.core.validators import MinValueValidator
from utils.text import get_random_name


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password,**extra_fields):
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,**extra_fields)
        user.set_password(password)
        user.save(using=self.db)
        return user
    
    def create_user(self,email,password=None,**extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email,password,**extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('fec_nacimiento','1111-11-11 11:11:11.000000-05')
        extra_fields.setdefault('telefono','111111111')
        extra_fields.setdefault('sexo','admin')
        extra_fields.setdefault('num_documento_identidad','00000000')\
        
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True')
        
        return self._create_user(email,password,**extra_fields)


class Rol(models.Model):
    name = models.CharField(max_length=255,blank=True)
    es_vigente = models.BooleanField(default=True)
    fec_reg = models.DateTimeField(auto_now_add=True)
    fec_mod = models.DateTimeField(auto_now=True)
    es_eliminadoo = models.BooleanField(default=False)

    class Meta:
        db_table = 'public\".\"rol'
        ordering = ['-id']
        get_latest_by = '-id'


def get_user_image(instance, filename):
        return 'user/{0}'.format(get_random_name(filename))

class User(AbstractUser):
    username = None
    is_active = models.BooleanField(default=True)
    foto = models.ImageField(upload_to=get_user_image, default='user/default.png')
    roles = models.ManyToManyField(
        Rol,
        through='UserRol',
        through_fields = ('user','rol')
    )
    fec_mod = models.DateTimeField(auto_now=True)
    es_eliminado = models.BooleanField(default=False)
    num_documento_identidad = models.CharField(max_length=15)
    sexo = models.CharField(max_length=8, null=True)
    email = models.EmailField(_('email address'),unique=True)
    fec_nacimiento = models.DateTimeField( null=True)
    is_anfitrion = models.BooleanField(default=False)
    telefono = models.CharField(max_length=11)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    class Meta:
        db_table = 'public\".\"user'
        ordering = ['-date_joined']
        get_latest_by = '-date_joined'
    
    def has_roles(self, role):
        if not isinstance(role, list):
            role = [role]

        return self.groups.all().filter(pk__in=role).exists() 







class UserRol(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)
    fec_init = models.DateField()
    fec_fin = models.DateField(null=True)
    fec_reg = models.DateTimeField(auto_now_add=True)
    fec_mod = models.DateTimeField(auto_now=True)
    es_eliminado = models.BooleanField(default=False)
    

    class Meta:
        db_table = 'public\".\"user_rols'
        ordering = ['-fec_init']
        get_latest_by = '-fec_init'


class TablaMaestra(models.Model):
    codigo = models.CharField(max_length=6)
    abreviatura = models.CharField(max_length=50)
    denominacion = models.TextField()
    es_vigente = models.BooleanField(default=True)
    usuario_reg = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_reg")
    fec_reg = models.DateTimeField(auto_now_add=True)
    usuario_mod = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_mod")
    fec_mod = models.DateTimeField(auto_now=True)
    es_eliminado = models.BooleanField(default=False)

    class Meta:
        db_table = 'public\".\"tabla_maestra'
        ordering = ['denominacion']
        get_latest_by = '-fec_reg'


def get_config_image(instance, filename):
    return 'system/{0}'.format(get_random_name(filename))

class Configuracion(models.Model):
    codigo_seccion = models.CharField(max_length=3)
    codigo_item = models.CharField(max_length=3)
    codigo = models.CharField(max_length=6)
    titulo = models.CharField(max_length=50)
    valor = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=250, null=True)
    imagen = models.ImageField(upload_to=get_config_image, null=True, max_length=255)
    es_vigente = models.BooleanField(default=True)
    usuario_reg = models.ForeignKey(settings.AUTH_USER_MODEL, 
        on_delete=models.CASCADE, related_name="configuracion_reg")
    fec_reg = models.DateTimeField(auto_now_add=True)
    usuario_mod = models.ForeignKey(settings.AUTH_USER_MODEL, 
        on_delete=models.CASCADE, related_name="configuracion_mod")
    fec_mod = models.DateTimeField(auto_now=True)
    es_eliminado = models.BooleanField(default=False)

    class Meta:
        db_table = 'public\".\"configuracion'
        ordering = ['-fec_reg']
        get_latest_by = '-fec_reg'





