from django.db import router
from django.urls import path 
from rest_framework import routers


from apps.public.Api.viewset.asign_role_viewset import UserRolViewSet

from apps.public.Api.viewset.list_user_viewset import ListUserViewSet
from apps.public.Api.viewset.rol_viewset import RolViewSet
#from apps.public.Api.viewset.user_rol_viewset import UserRolAgenciaViewSet

from .Api.viewset.user_viewset import UserViewSet 
#from .Api.viewset.tabla_maestra_viewset import TablaMaestraViewSet
#from .Api.viewset.register_viewset import RegisterViewSet

from .Api.viewset.registrar_viewset import RegisterViewSet

from . import views

app_name = 'public'

route = routers.SimpleRouter()

route.register('users', UserViewSet)
#router.register('tablas_maestras', TablaMaestraViewSet)
#route.register('register', RegisterViewSet, basename='register')
route.register('asign_rol', UserRolViewSet)
route.register('roles', RolViewSet)
route.register('list_user', ListUserViewSet)
route.register('register', RegisterViewSet, basename='register')
##route.register('config', ConfiguracionViewset)
##route.register('agencia_rol', UserRolAgenciaViewSet)


urlpatterns = []

urlpatterns +=route.urls