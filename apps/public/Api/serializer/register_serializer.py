from django.contrib.auth.models import Group

from django.db import transaction
from rest_framework import exceptions, serializers, validators

from ...models import User



class CreateUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = '__all__'
        
    
    def create(self, validated_data):
        
        return User.objects.create_user(
             **validated_data
             
         )


        
        



