from django.db import transaction
from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions, serializers
from ...models import User, Rol
from ..serializer.group_serializer import GroupSerializer

class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, read_only = True)

    class Meta:
        model = User
        exclude = ['password','user_permissions']

class RoleSerializer(serializers.Serializer):
    class Meta:
        model =  Rol
        exclude = ['user_reg','user_mod']


class LoginUserSerializer(serializers.ModelSerializer):
    # Serializer used in custom token response
    groups = GroupSerializer(many=True)
    roles = RoleSerializer(many=True)
    foto = serializers.ImageField()
    class Meta:
        model = User
        fields = [
            'id', 'first_name', 'last_name', 'email',
            'foto', 'groups', 'roles'
        ]

class BasicUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields =['id', 'first_name', 'last_name', 'foto']


##Changing passwd
class UserPasswordSerializer(serializers.Serializer):
    password = serializers.CharField()
    newPassword = serializers.CharField(
        min_length=8)
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault())

    def update(self, instance: User, validated_data):
        if not instance.check_password(validated_data.get('password')):
            raise exceptions.ValidationError(
                _('The password given does not match.'))
        instance.set_password(validated_data.get('newPassword'))
        instance.save()
        return instance


class AdminPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(
        min_length=8)
    user = serializers.PrimaryKeyRelatedField(
        queryset=User.objects.prefetch_related('groups').all())
    current_user = serializers.HiddenField(
        default=serializers.CurrentUserDefault())


    def create(self, validated_data):
        user: User = validated_data.pop('user')
        user.set_password(validated_data.get('password'))
        user.save()
        return user