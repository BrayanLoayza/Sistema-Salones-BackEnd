from dataclasses import field
from django.contrib.auth.models import Group
from rest_framework import serializers

class GroupSerializer(serializers.Serializer):
    class Meta:
        model = Group
        fields = ['id', 'name']