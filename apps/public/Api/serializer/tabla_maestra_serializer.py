from rest_framework.exceptions import ValidationError
from ...models import TablaMaestra 
from rest_framework import serializers

class ListTablaMaestraSerializer(serializers.ModelSerializer):
    class Meta:
        model = TablaMaestra
        fields = "__all__"
    

class CreateUpdateTablaMaestraSerializer(serializers.ModelSerializer):
    usuario_reg = serializers.HiddenField(
        default=serializers.CurrentUserDefault())
    usuario_mod =serializers.HiddenField(
        default=serializers.CurrentUserDefault())
    class Meta:
        model = TablaMaestra
        fields = ["codigo", "abreviatura", "denominacion", "usuario_reg", "usuario_mod"]

    

    def update(self, instance, validated_data):
        instance.usuario_mod = self.context['request'].user
        instance.save()
        return instance


                   