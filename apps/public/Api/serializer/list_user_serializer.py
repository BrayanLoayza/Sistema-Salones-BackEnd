from dataclasses import field
from django.utils.translation import gettext_lazy as _
from rest_framework import exceptions, serializers

from ...models import User
from ..serializer.group_serializer import GroupSerializer

class UserSerializer(serializers.Serializer):
    groups = GroupSerializer(many=True, read_only=True)

    class Meta:
        model = User
        exclude = ['password', 'user_permissions']

class ListUserSerializer(serializers.ModelSerializer):
    foto=serializers.ImageField()
    class Meta:
        model = User
        fields = [
            'id', 'email',
            'foto', 'date_joined',
            'is_active', 'foto',
            'fec_mod', 'es_eliminado'

        ]


        