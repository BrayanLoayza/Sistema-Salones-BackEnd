
from django.contrib.auth.models import Group

from django.db import transaction
from rest_framework import exceptions, serializers, validators

from ...models import User


class CreateUserSerializer(serializers.Serializer):
    email=serializers.EmailField()
    nombre=serializers.CharField(max_length=30)
    #apellido_paterno=serializers.CharField(max_length=30)
    #apellido_materno=serializers.CharField(max_length=30)
    apellidos = serializers.CharField(max_length=30)
    
    #foto = serializers.ImageField(max_length=None)
   
    fec_nacimiento = serializers.DateTimeField( )
  
    password = serializers.CharField(
        max_length=50, write_only=True)
    sexo = serializers.CharField(max_length=8)
    telefono = serializers.CharField(max_length=12)

    def create(self, validated_data):
        #lastname = validated_data.get('apellido_paterno')+validated_data.get('apellido_materno')
        foto = validated_data.get('foto')

        with transaction.atomic():
            user: User = User.objects.create_user(
                    email=validated_data.get('email'),
                    first_name=validated_data.get('nombre'),
                    last_name=validated_data.get('apellidos'),               
                   
                    password=validated_data.get('password'),
                    #foto=validated_data.get('foto'),
                    
                    fec_nacimiento=validated_data.get('fec_nacimiento'),
                   
                    sexo=validated_data.get('sexo'),
                    telefono = validated_data.get('telefono')
     
                )
        
            return user


    
    
        #with transaction.atomic():
           # if foto is not None:
               # user: User = User.objects.create_user(
                   # email=validated_data.get('email'),
                    #first_name=validated_data.get('nombre'),
                    #last_name=lastname,                
                   
                    #password=validated_data.get('password'),
                    #foto=validated_data.get('foto'),
                    
                    #fec_nacimiento=validated_data.get('fec_nacimiento'),
                   
                    #sexo=validated_data.get('sexo'),
                    #telefono = validated_data.get('telefono')
     
               # )
            #else:
               # user: User = User.objects.create_user(
                   # email=validated_data.get('email'),
                   # first_name=validated_data.get('nombre'),
                   # last_name=lastname,                
                    
                   # password=validated_data.get('password'),
                    
                   # fec_nacimiento=validated_data.get('fec_nacimiento'),
                   
                    #sexo=validated_data.get('sexo'),
                   #telefono = validated_data.get('telefono')
                    
                #)

   