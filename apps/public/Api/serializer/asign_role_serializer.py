from dataclasses import fields
from msilib.schema import Class
from django.contrib.auth.models import Group
from rest_framework import serializers, validators
from ...models import UserRol, User, Rol

class createUserRolSerializer(serializers.Serializer):
    class Meta:
        model = UserRol
        fields = [
            'id'
            'user',
            'rol',
            'fec_init',
            'fec_fin',
            'fec_reg',
            'fec_mod',
            'es_eliminado'
        ]

class _rolSerializer(serializers.Serializer):
    class Meta:
        model = Rol
        fields = ['name']


class ListUserRolSerializer(serializers.Serializer):
    rol = _rolSerializer(many = False)
    class Meta:
        model = UserRol
        fields = [
            'id'
            'user',
            'rol',
            'fec_init',
            'fec_fin',
            'fec_reg',
            'fec_mod',
            'es_eliminado'
            
        ]