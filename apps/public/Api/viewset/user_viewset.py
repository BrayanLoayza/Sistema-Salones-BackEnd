from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied

from ...models import User
from ..serializer.user_serializer import (
    UserSerializer,
    UserPasswordSerializer,
    AdminPasswordSerializer,
)
from hall.constants import ROLE
from utils import permissions as custom_permission


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [custom_permission.GroupPermission]
    permission_groups = {
        'list': [custom_permission.ALLOW_ANY],
        'retrieve': [custom_permission.ALLOW_ANY],
        'create': [custom_permission.ALLOW_ANY],
        'partial_update': [custom_permission.IS_AUTHENTICATED],
        'destroy': [ROLE.ADMIN], 
        'change_passwd': [custom_permission.IS_AUTHENTICATED],
        'password_admin': [custom_permission.IS_AUTHENTICATED],
    }

    def get_serializer_class(self):
        if hasattr(self, 'action') and self.action == 'change_passwd':
            return UserPasswordSerializer
        elif hasattr(self, 'action') and self.action == 'password_admin':
            return AdminPasswordSerializer
        return super().get_serializer_class()

    @action(methods=['POST'], detail=False, url_path='change_passwd')
    def change_passwd(self, request, pk=None):
        serializer = self.get_serializer(self.request.user, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK)

    @action(methods=['POST'], detail=False, url_path='password_admin')
    def password_admin(self, request, pk=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_200_OK)