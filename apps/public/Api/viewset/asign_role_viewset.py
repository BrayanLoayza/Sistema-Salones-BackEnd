from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from ..serializer.asign_role_serializer import createUserRolSerializer, ListUserRolSerializer
from utils import permissions as custom_permission
from ...models import UserRol

class UserRolViewSet(viewsets.ModelViewSet):
    queryset = UserRol.objects.filter(es_eliminado=False)
    serializer_class = createUserRolSerializer
    permission_classes = [custom_permission.GroupPermission]
    permission_groups = {
        'list': [custom_permission.IS_AUTHENTICATED],
        'retrieve': [custom_permission.IS_AUTHENTICATED],
        'create': [custom_permission.IS_AUTHENTICATED],
        'partial_update': [custom_permission.IS_AUTHENTICATED],
    }

    filterset_fields = ['user']

    def get_serializer_class(self):        
        if hasattr(self, 'action') and self.action == 'list':
            return ListUserRolSerializer
        return super().get_serializer_class()

    def perform_create(self, serializer):
        serializer.save(user_mod=self.request.user, user_reg=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user_mod=self.request.user)