from rest_framework import viewsets

from ..serializer.rol_serializer import RolSerializer
from utils import permissions as custom_permission
from ...models import Rol

class RolViewSet(viewsets.ModelViewSet):
    queryset = Rol.objects.all()
    serializer_class = RolSerializer  
    permission_classes = [custom_permission.GroupPermission]
    permission_groups = {
        'list': [custom_permission.ALLOW_ANY],
    }


