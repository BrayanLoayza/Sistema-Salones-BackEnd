from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from ..serializer.register_serializer import CreateUserSerializer
from utils import permissions as custom_permission
from ...models import User

class RegisterViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = CreateUserSerializer   
    permission_classes = [custom_permission.GroupPermission]
    permission_groups = {
        'generics': [custom_permission.ALLOW_ANY],
        'list': [custom_permission.ALLOW_ANY],
        'verify_username': [custom_permission.ALLOW_ANY],
        'create': [custom_permission.ALLOW_ANY],
    }

    
    def get_serializer(self):
        if hasattr(self, 'action') and self.action == 'generics':
            return CreateUserSerializer
    
    

    @action(methods=['POST'], detail=False, url_path='verify_username')
    def verify_username(self, request):
        name=request.data.get('email')
        valor = True
        if User.objects.filter(email=name).exists():
            valor= True
        else:
            valor= False
        
        return Response(data={'exist': valor}, status=status.HTTP_200_OK)
    
    

   


        
        


