from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from ..serializer.registrar_serializer import CreateUserSerializer

from utils import permissions as custom_permission
from ...models import User

class RegisterViewSet(viewsets.GenericViewSet):
    queryset = User.objects.all()    
    permission_classes = [custom_permission.GroupPermission]
    permission_groups = {
        'generics': [custom_permission.ALLOW_ANY],
        'verify_email': [custom_permission.ALLOW_ANY],
        'list':[custom_permission.ALLOW_ANY],
    }

    def get_serializer_class(self):
        if hasattr(self, 'action') and self.action == 'generics':
            return CreateUserSerializer
        
    # def _create(self, request, *args, **kwargs):
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_create(serializer)
    #     headers = self.get_success_headers(serializer.data)
    #     return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(methods=['POST'], detail=False, url_path='verify_email')
    def verify_email(self, request):
        name=request.data.get('email')
        valor = True
        if User.objects.filter(email=name).exists():
            valor= True
        else:
            valor= False
        
        return Response(data={'exist': valor}, status=status.HTTP_200_OK)

    @action(methods=['POST'], detail=False, url_path='generics')
    def generics(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        datos = {'id': serializer.save().id}
        return Response(data=datos, status=status.HTTP_201_CREATED)

    