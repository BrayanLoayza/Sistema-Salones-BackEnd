from rest_framework import viewsets

from ...models import User
from ..serializer.list_user_serializer import (
    
    ListUserSerializer
)
from utils import permissions as custom_permission


class ListUserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = ListUserSerializer
    permission_classes = [custom_permission.GroupPermission]
    permission_groups = {
        'list': [custom_permission.IS_AUTHENTICATED],
        'retrieve': [custom_permission.IS_AUTHENTICATED],
        'partial_update': [custom_permission.IS_AUTHENTICATED],
    }

    

    def perform_update(self, serializer):
        instance = serializer.save()
        instance.natural.usuario_mod = self.request.user
        instance.natural.save()

