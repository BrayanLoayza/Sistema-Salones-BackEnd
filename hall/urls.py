"""hall URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path


from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from apps import public
from apps.public import urls
from rest_framework_simplejwt.views import (
    TokenRefreshView,
    TokenVerifyView
)
from .token import CustomObtainPairView

urlpatterns = [
    #auth urls
    path('sde/auth/login', CustomObtainPairView.as_view(), name='token-obtain'),
    path('sde/auth/refresh', TokenRefreshView.as_view(), name='token-refresh'),
    path('sde/auth/verify', TokenVerifyView.as_view(), name='token-verify'),

    #apps
    path('sde/', include('apps.public.urls')),



    
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT )


if settings.DEBUG:
    urlpatterns += [
        path('admin/', admin.site.urls),
        path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
        #debug
        path('__debug__/', include('debug_toolbar.urls')),
    ]
