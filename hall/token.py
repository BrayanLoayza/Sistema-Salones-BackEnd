from django.utils import timezone
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from rest_framework import status
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.exceptions import TokenError, InvalidToken

from apps.public.Api.serializer.user_serializer import LoginUserSerializer


class CustomObtainPairView(TokenObtainPairView):
    """
    Ovewrite the `TokenObtainPairView.post` method for add user information
    to response and update user last_login.
    """

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
            if (not(serializer.user.is_active)):
                raise ValidationError("La cuenta se encuentra desactivada")               
            
        except TokenError as e:
            raise InvalidToken(e.args[0])
        data = dict(**serializer.validated_data, **LoginUserSerializer(serializer.user).data)
        serializer.user.last_login = timezone.now()
        serializer.user.save()

        return Response(data=data, status=status.HTTP_200_OK)
