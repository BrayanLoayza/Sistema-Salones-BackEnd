import os

from django.conf import settings
from django.http import HttpResponse
from django.utils import timezone
from rest_framework.views import exception_handler


# `detail` is for default DRF exception detail
DEFAULT_ERRORS = [settings.NON_FIELD_ERRORS_KEY, 'detail']
LOG_DIR = settings.BASE_DIR / 'log' / 'validation'


def _create_log(exc, context):
    now = str(timezone.now())
    file = LOG_DIR / f'{now[0:10]}.txt'
    if not os.path.exists(LOG_DIR):
        os.makedirs(LOG_DIR)
    if not os.path.exists(file):
        with open(file, 'w') as f:
            f.write('\t\t\t\t\t###################################\n')
            f.write(f'\t\t\t\t\t###  VALIDATION LOG {now[0:10]}  ###')
            f.write('\n')
            f.write('\t\t\t\t\t###################################\n')
    with open(file, 'a') as f:
        f.write('\n')
        f.write(f'{now[0:10]} {now[11:23]}\n')
        f.write(f'Request: {context["request"]}\n')
        f.write(f'User: {context["request"].user}\n')
        f.write(f'Exeption: {exc}\n')


def _format_response(response: HttpResponse) -> HttpResponse:
    """
    Format response to send validators message in a structured form.
    """
    new_data = {}
    parsed = []

    # Case for some validations exceptions when not pass a object
    # when its raised
    if isinstance(response.data, list):
        return response
    # Check if exists a `detail` or `NON_FIELD_ERROR`. If exists, copy its value to
    # a `error` atribute and set `messages` to []
    for e in DEFAULT_ERRORS:
        if e in response.data.keys():
            new_data['error'] = response.data[e] if isinstance(response.data[e], str) else response.data[e][0]
            new_data['messages'] = None
            response.data = new_data
            return response

    for key in response.data:
        _errors = [_ for _ in response.data[key]] if isinstance(response.data[key], list) else [response.data[key]]
        # Parsed for ListField serializer exceptions
        if isinstance(_errors[0], dict):
            _errors_messages = []
            for k in _errors[0].keys():
                if isinstance(_errors[0][k], list):
                    _errors_messages += _errors[0][k]
            _errors = _errors_messages

        parsed.append({
            'property': key,
            'errors': _errors
        })

    new_data['error'] = None
    # For view the normal and parsed data together, comment and uncomment the following 3 lines
    # response.data['message'] = parsed
    new_data['messages'] = parsed
    response.data = new_data
    return response


def custom_exception_handler(exc, context):
    # exc: original exception
    # context: extra info such as view, args, kwargs, request
    response = exception_handler(exc, context)
    if response is not None:
        _create_log(exc, context)
        response = _format_response(response)
    return response
