import os
from pathlib import Path

from django.conf import settings
from django.utils import timezone
from django.views.debug import ExceptionReporter


LOG_DIR = settings.BASE_DIR / 'log'


class LogExceptionReporter(ExceptionReporter):
    """Custom 'ExceptionReporter' for save errors in log file."""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._now = str(timezone.now())

    @property
    def _current_file(self) -> Path:
        return LOG_DIR / f'{self._now[0:10]}.txt'

    def _validate_dir(self) -> Path:
        """Verify if log dir and files exists, else create it."""
        if not os.path.exists(LOG_DIR):
            os.mkdir(LOG_DIR)
        if not os.path.exists(self._current_file):
            # os.mknod(self._current_file)
            with open(self._current_file, 'w') as f:
                f.write('\t\t\t\t\t##################################\n')
                f.write(f'\t\t\t\t\t###  EXCEPTION LOG {self._now[0:10]}  ###')
                f.write('\n')
                f.write('\t\t\t\t\t##################################\n')

    def get_traceback_data(self):
        """
        Return a dictionary containing traceback information.

        Dict returned is the same as the parent class, only add
        functionality for save some traceback information in a log file.
        """
        # For more exception information, check the ExceptionReporter
        # get_traceback_data method.
        data = super().get_traceback_data()
        self._validate_dir()
        with open(self._current_file, 'a') as f:
            f.write('\n')
            f.write(f'{self._now[0:10]} {self._now[11:23]}\n')
            #f.write(f'Request: {data.get("request_GET_items")}\n') exception_type
            f.write(f'Request Meta: {data.get("request_meta")}\n')
            f.write(f'Post: {data.get("filtered_POST_items")}\n')
            f.write(f'path: {data.get("sys_path")}\n')
            f.write(f'Request: {data.get("request")}\n')
            f.write(f'Tipo: {data.get("exception_type")}\n')
            f.write(f'Exception: {data.get("exception_value")}\n')
            f.write(f'User: {data.get("user_str")}\n')
        return data


class LogReporter():
    """Custom 'ExceptionReporter' for save errors in log file."""
    def __init__(self, *args, **kwargs):
        self._now = str(timezone.now())

    @property
    def _current_file(self) -> Path:
        return LOG_DIR / f'{self._now[0:10]}.txt'

    def _validate_dir(self) -> Path:
        """Verify if log dir and files exists, else create it."""
        if not os.path.exists(LOG_DIR):
            os.mkdir(LOG_DIR)
        if not os.path.exists(self._current_file):
            # os.mknod(self._current_file)
            with open(self._current_file, 'w') as f:
                f.write('\t\t\t\t\t##################################\n')
                f.write(f'\t\t\t\t\t###  EXCEPTION LOG {self._now[0:10]}  ###')
                f.write('\n')
                f.write('\t\t\t\t\t##################################\n')

    def get_traceback_data(self, msg: str) -> None:
        """

        Dict returned is the same as the parent class, only add
        functionality for save some traceback information in a log file.
        """
        # For more exception information, check the ExceptionReporter
        # get_traceback_data method.
        self._validate_dir()
        with open(self._current_file, 'a') as f:
            f.write('\n')
            f.write(f'{self._now[0:10]} {self._now[11:23]}\n')
            f.write(f'TIPO: CALCULO DE IMPUESTOS\n')
            #f.write(f'Request: {data.get("request_GET_items")}\n') exception_type
            f.write(f'Mensaje: {msg}\n')

